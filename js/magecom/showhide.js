/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package js
 * @copyright Copyright (c) 2015 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Magecom StudyTasks js
 *
 * @category    Magecom
 * @package     Magecom_StudyTasks
 * @author      Magecom, Inc.
 */
Event.observe(window, 'load', function() {
   Showhide.prototype.init();
});

Showhide = Class.create();
Showhide.prototype = {
    
    showed : false,
    
    init: function()
    {
        this.button   = $('showhide');
        this.action   = window.location.origin + '/studytasks/studytable/showhide';
        Event.observe(this.button, 'click', this.showContent.bind(this));
    },
    
    showContent: function () 
    {
        if(this.showed == false) {
            this.showed = true;
            new Ajax.Request(
            this.action, {
                method     : "post",
                onSuccess : function(myData) {
                    var res =   myData.responseText;
                    $('showhide_content').update(res);
                    $('showhide').innerHTML = 'Hide';
                }
            });
        } else {
            this.showed = false;
            this.button.innerHTML = 'Show';
            $('showhide_content').update('');
        }
    }
};




