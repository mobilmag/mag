/**
 * Magecom
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@magecom.net so we can send you a copy immediately.
 *
 * @category Magecom
 * @package js
 * @copyright Copyright (c) 2015 Magecom, Inc. (http://www.magecom.net)
 * @license  http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Magecom StudyTasks js
 *
 * @category    Magecom
 * @package     Magecom_StudyTasks
 * @author      Magecom, Inc.
 */
Event.observe(window, 'load', function() {
   Subscriber.prototype.init();
});

Subscriber = Class.create();
Subscriber.prototype = {
    
    init: function()
    {
        this.email  = $('email');
        this.submit = $('submit');
        this.form   = $('subscribtion-validate-detail');
        this.formId = Element.identify(this.form);
        this.action = this.form.action;
        this.newValidations();

        //Event.observe('submit', 'click', this.subscribe.bind(this));
        Event.observe('submit', 'click', function(e){
            e.stop();
            this.subscribe();
        }.bind(this));
    },

    subscribe: function () {
        var myForm = new VarienForm(this.formId, true);
        var response;
        if (myForm.validator.validate()) {
            new Ajax.Request(
                this.action, {
                    method     : "post",
                    parameters : {email: this.email.value},
                    onSuccess : function(myData) {
                        var res =   myData.responseText;
                        var notice = $('email_notice');
                        notice.update(res);
                    }
                }
            );
        }
    },
    
    newValidations : function()
    {
        Validation.add('validate-digits',Translator.translate('Please enter digits only'),function(the_field_value){
            var regex = /^[0-9]+$/;
            return the_field_value.match(regex) ? true : false;
        });    
        Validation.add('validate-compare',Translator.translate('Filed1 has to be equal to Field2'),function(the_field_value){
            return $('field1').getValue() == the_field_value ? true : false;
        });    
    }
};